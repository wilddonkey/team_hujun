/**
 * Created by Administrator on 2019/8/28.
 */
var baseURL = 'http://localhost/findimg?fileName';

function ajax(url,obj,s,e){
    $.ajax({
        type: 'post',
        url: url,
        data: JSON.stringify(obj),
        contentType: 'application/json;charset=utf-8',
        success: s,
        error:e || function(ex){
            console.log(ex)
        }
    });
}