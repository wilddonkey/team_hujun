function jssdk(f,jsApiList){
    $.get("find_config",{url:
        encodeURIComponent(location.href.split('#')[0])},function(data){
        wx.config({
            debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
            appId:data.appId, // 必填，公众号的唯一标识
            timestamp:data.timestamp.substring(10) , // 必填，生成签名的时间戳
            nonceStr: data.nonceStr.substring(9), // 必填，生成签名的随机串
            signature: data.signature.toLowerCase(),// 必填，签名
            jsApiList: jsApiList // 必填，需要使用的JS接口列表
        });
    });

    wx.ready(function(){
        f();
    });
}