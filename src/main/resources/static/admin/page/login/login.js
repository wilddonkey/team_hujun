layui.config({
    base: "js/"
}).use(['form', 'layer'], function () {
    var form = layui.form(),
        layer = parent.layer === undefined ? layui.layer : parent.layer,
        $ = layui.jquery;
    //video背景
    $(window).resize(function () {
        if ($(".video-player").width() > $(window).width()) {
            $(".video-player").css({
                "height": $(window).height(),
                "width": "auto",
                "left": -($(".video-player").width() - $(window).width()) / 2
            });
        } else {
            $(".video-player").css({
                "width": $(window).width(),
                "height": "auto",
                "left": -($(".video-player").width() - $(window).width()) / 2
            });
        }
    }).resize();


    //登录按钮事件
    form.on("submit(login)", function (data) {
        //alert(JSON.stringify(data));
        $.post(
            "/login",
            data.field,
            function (result) {
                //alert(result.result);
                if (result.result) {
                    //alert(JSON.stringify(result.admin))
                    layer.msg(result.msg);
                    sessionStorage.setItem("User_Login",JSON.stringify(result.admin));
                    window.location.href = "../../index.html";
                } else {
                    layer.msg(result.msg);
                    window.reload();
                }
            }
        );
        /*window.location.href = "../../index.html";*/
        return false;
    })
})
