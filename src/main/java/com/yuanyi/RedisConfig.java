package com.yuanyi;

import org.redisson.Redisson;
import org.redisson.config.Config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RedisConfig {
    @Bean
    public Redisson redisson () {
        Config cfg = new Config();
        cfg.useSingleServer().
                setAddress("redis://localhost:6379")
                .setDatabase(0);
        return (Redisson) Redisson.create(cfg);
    }
}