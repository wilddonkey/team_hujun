package com.yuanyi.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Administrator on 2019/8/26.
 */
public class NetUtil {
    /**
     * 向网址发送POST 请求
     * @param urlStr 网址
     * @param jsonStr JSON字符串
     * @return 该网址返回给我的JSON对象
     */
    public static JSONObject post(String urlStr, String jsonStr){
        try {
            URL url = new URL(urlStr);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setDoOutput(true);
            con.setDoInput(true);

            con.getOutputStream().write(jsonStr.getBytes());
            con.getOutputStream().flush();

            BufferedReader br = new BufferedReader(
                    new InputStreamReader(con.getInputStream(),"utf-8")
            );

            String str;
            StringBuffer sb = new StringBuffer();
            while((str=br.readLine())!=null){
                sb.append(str);
            }

            return JSON.parseObject(sb.toString());

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static String get(String urlStr){
        try {
            URL url = new URL(urlStr);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();

            BufferedReader br = new BufferedReader(
                    new InputStreamReader(con.getInputStream(),"utf-8")
            );

            String str;
            StringBuffer sb = new StringBuffer();
            while((str=br.readLine())!=null){
                sb.append(str);
            }

            return sb.toString();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static JSONObject sendWechatMsg(String accessToken,String openid,String url){
        JSONObject obj =  NetUtil.post("https://api.weixin.qq.com/cgi-bin/message/template/send?access_token="
                +accessToken,
                " {\n" +
                        "           \"touser\":\""+openid+"\",\n" +
                        "           \"template_id\":\"o_2pIDbALDHp9KNsH16pkl2DrattTYy0Ug30XluXIcs\",\n" +
                        "           \"url\":\""+url+"\",  \n" +
//                        "           \"miniprogram\":{\n" +
//                        "             \"appid\":\"xiaochengxuappid12345\",\n" +
//                        "             \"pagepath\":\"index?foo=bar\"\n" +
                        //   "           },          \n" +
                        "           \"data\":{\n" +
                        "                   \"first\": {\n" +
                        "                       \"value\":\"亲爱的工程师，有客户下单，请尽快抢单！\",\n" +
                        "                       \"color\":\"#173177\"\n" +
                        "                   },\n" +
                        "                   \"track_number\":{\n" +
                        "                       \"value\":\"000001\",\n" +
                        "                       \"color\":\"#173177\"\n" +
                        "                   },\n" +
                        "                   \"asp_name\": {\n" +
                        "                       \"value\":\"合肥蜀山区\",\n" +
                        "                       \"color\":\"#173177\"\n" +
                        "                   },\n" +
                        "                   \"asp_tel\": {\n" +
                        "                       \"value\":\"13899888899\",\n" +
                        "                       \"color\":\"#173177\"\n" +
                        "                   },\n" +
                        "                   \"remark\":{\n" +
                        "                       \"value\":\"如需帮助，请回复“rgfw”！\",\n" +
                        "                       \"color\":\"#173177\"\n" +
                        "                   }\n" +
                        "           }\n" +
                        "       }");
       return obj;
    }
}
