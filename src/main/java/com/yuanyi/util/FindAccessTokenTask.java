package com.yuanyi.util;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;

@Component
public class FindAccessTokenTask {

    @Resource
    RedisTemplate<String,Object> redisTemplate;

    @Scheduled(fixedRate = 1000*60*60*2)
    public void find() throws Exception {
        String accessToken = (String) redisTemplate.opsForValue().get("access_token");
        if(accessToken==null) {
            AccessTokenUtil.findAccessToken(redisTemplate);
        }

        
    }
}
