package com.yuanyi.util;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.model.OSSObject;

import javax.servlet.http.HttpServletResponse;
import java.io.*;

/**
 * Created by Administrator on 2019/8/30.
 */
public class OSSUtil {
    public static void main(String[] args) throws IOException {
     //   ossUpload("pic1","data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAkCAYAAADy19hsAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyhpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTM4IDc5LjE1OTgyNCwgMjAxNi8wOS8xNC0wMTowOTowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTcgKE1hY2ludG9zaCkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6Mzk0ODc4MUIzNEU1MTFFNzkwMjdBQUE2OTE5RjZBMUQiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6Mzk0ODc4MUMzNEU1MTFFNzkwMjdBQUE2OTE5RjZBMUQiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDozOTQ4NzgxOTM0RTUxMUU3OTAyN0FBQTY5MTlGNkExRCIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDozOTQ4NzgxQTM0RTUxMUU3OTAyN0FBQTY5MTlGNkExRCIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/Pt4+95UAAAMLSURBVHja7JhraxNBFIY3NY3FS5piqSFtYysq1qbgDYsYwYpX1H9Q/J2ltbFC0S8qgjam9otGE6r10iZRQq2J75R3YdjOzM4mMRjIgQeyu3N5Z+bMnDMJ1et1p5Osx+kw6wruCu50wWH5IRQKNTroOBgGgyAGDoB94A/4BTbBV1AA66AWtBP3NAs3Mdh+cAacBCXwCaxQXBVsgwjo4yCGwCXWeweyYCtopyH5HLac4UPgIkiCt2CVgm3tMDgNJkAePAcV2xkOIlh8TIFzFPoK/G5ihXrBWQoXbb0WulolWPjkdS7xY/BDU26AszfKmRRWBh9BTlNP1JmhC2Xo800JFhvpNv3uhWbDiI13GRznTOUl/+yn+0yB9+AZ2FHUvwBOgXmwoRO8+8NFYSPgIRj3OSXugZtcAZ1FWOa+4TgdZ3+jKsG7Gg2Ck6wc9/HFNLgVwHeF6CuG73H2m1QJ1o00Qb+a57npGHxWzMqS5xSZBg/INN+59oR1BjRtrrPfGerwjXRRFl7wEetwg61ww7hirzGI9JJhvnNFi7JvWNcxiF6gjqhJsPCzu9xcRYvlFb72QXqe1PhxhN9cy6v81GNF6rjDgSsFX2X4zFn6Y5SRzbUhQ9mj0u9N78xpLEfhaZ3gGnOAf21unmFbVusSy5yJlGVjJeYJrn0xlP3sCc8Vi/ZT1LOsEyxC7RzDb8KiQRHBjknPWWkDyrbNb/KRWfBpO0Edc3IK0KOZtUVww+N3KltlxhaRwvASxeyQAt+VpQ2Y8tkncfa/uCexMgQOEeVmLUSnGQyCBI60z+acZf/WgcNhfpthHjFmKPdUCrs2oXk/66hsjP1l2H9D+fAgl2cNvNSkgKrkxz3uYpbJz3lwAjzi7aSp9LKPUSfM0FoyhOoJLmeUx2RZOtu/K+rE2HaVqWu1lQn8FJPu/z6B112Rspy5SgChbbsiqS6hk7yEbvFM3vC5hI6w3hqTJutLaCsEe6/5QswRijvI98KHf3IQ37jzW3fNb/CfzBoTlKLTBuv+VdUV3BXcZvsrwADU1x297QTiiQAAAABJRU5ErkJggg==");
      //  System.out.println(OSSUtil.ossDownload("bf41bc5e-a153-4d7a-8d06-3ee4abbecdac.jpg"));;
    }

    //URL https://wx16.oss-cn-qingdao.aliyuncs.com/pic1
    public static void ossUpload(String picName,byte[] b){
        // Endpoint以杭州为例，其它Region请按实际情况填写。
        String endpoint = "http://oss-cn-qingdao.aliyuncs.com";
// 阿里云主账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM账号进行API访问或日常运维，请登录 https://ram.console.aliyun.com 创建RAM账号。
        String accessKeyId = "8XierCVyOqtyk0pN";
        String accessKeySecret = "IoLVusEjSmLxXK72ykTOftqMoMwE3i";
        String bucketName = "wx16";
        String objectName = picName;

// 创建OSSClient实例。
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);

// 上传内容到指定的存储空间（bucketName）并保存为指定的文件名称（objectName）。

        ossClient.putObject(bucketName, objectName,
                new ByteArrayInputStream(b));

// 关闭OSSClient。
        ossClient.shutdown();
    }

    public static void ossDownload(HttpServletResponse response, String picName) throws IOException {
        // Endpoint以杭州为例，其它Region请按实际情况填写。
        String endpoint = "http://oss-cn-qingdao.aliyuncs.com";
// 阿里云主账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM账号进行API访问或日常运维，请登录 https://ram.console.aliyun.com 创建RAM账号。
        String accessKeyId = "8XierCVyOqtyk0pN";
        String accessKeySecret = "IoLVusEjSmLxXK72ykTOftqMoMwE3i";
        String bucketName = "wx16";
        String objectName = picName;
// 创建OSSClient实例。
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);

// ossObject包含文件所在的存储空间名称、文件名称、文件元信息以及一个输入流。
        OSSObject ossObject = ossClient.getObject(bucketName, objectName);
// 读取文件内容。
        //System.out.println("Object content:");
        InputStream in = ossObject.getObjectContent();

        int num;
        byte[] b= new byte[1024];
        while((num=in.read(b))!=-1) {
            response.getOutputStream().write(b,0,num);
        }
        response.getOutputStream().flush();

        ossClient.shutdown();
    }
}
