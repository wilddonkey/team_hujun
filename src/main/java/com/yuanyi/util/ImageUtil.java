package com.yuanyi.util;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by Administrator on 2019/8/28.
 */
public class ImageUtil {
    public static BufferedImage zoomPic(String oldPic,double rate){
        BufferedImage img = null;
        try {
            img = ImageIO.read(new File(oldPic));
            int w = img.getWidth();
            int h = img.getHeight();
            int sw = (int) (w*rate);
            int sh = (int) (h*rate);
            Image image = img.getScaledInstance(sw,sh,1);
            BufferedImage simg = new BufferedImage(sw,sh,
                    BufferedImage.TYPE_INT_RGB);
            simg.getGraphics().drawImage(image,0,0,null);
//        simg.getGraphics().setColor(Color.orange);
//        simg.getGraphics().setFont(new Font("宋体",30,Font.BOLD));
//        simg.getGraphics().drawString("59快修门市部",100,100);
            return simg;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;

    }
}
