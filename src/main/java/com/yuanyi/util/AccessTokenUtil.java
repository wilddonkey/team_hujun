package com.yuanyi.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.databind.util.JSONPObject;
import org.springframework.data.redis.core.RedisTemplate;


import javax.annotation.Resource;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.TimeUnit;

/**
 * Created by Administrator on 2019/8/23.
 */
public class AccessTokenUtil {
    public static String accessToken;
   public static final String APPID = "wx9cfb23748200cfe0";
   public static final String APPSECRET = "f1f766cfff668c5f73770dc10d533dce";

//       public static final String APPID = "wx799078a4ead39219";
//   public static final String APPSECRET = "3ab509c21658caed6846d757ec104338";



//    appID
//            wx799078a4ead39219
//    appsecret
//3ab509c21658caed6846d757ec104338



    public static void main(String[] args)throws Exception {
//        URL url = new URL("https://api.weixin.qq.com/cgi-bin/token?" +
//                "grant_type=client_credential&appid="+APPID+"&secret="+APPSECRET);
//        HttpURLConnection con = (HttpURLConnection) url.openConnection();
//
//        BufferedReader br = new BufferedReader(
//                new InputStreamReader(con.getInputStream(),"utf-8")
//        );
//
//        String str;
//        StringBuffer sb = new StringBuffer();
//        while((str=br.readLine())!=null){
//            sb.append(str);
//        }
//
//        JSONObject obj =  JSON.parseObject(sb.toString());
//        System.out.println(obj.getString("access_token"));

//       JSONObject obj= NetUtil.post("https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=24_rubD9j0w1aPLW4gDsMjfKJSxEaWoIJ-hGS98soEfddB2mRLHxWRHmd9FBwLTa-14ATLyEo1RhFlmpsF7clcmT8IArG5Yj-LDQAT00muQreNf1cikFjMXbV9BxW7m34OzyzhAFL1Wab2vRnFgITVbABAPAC"
//                ,"{\n" +
//                        "\n" +
//                        "    \"touser\":\"ogvh9w9ovfY0uqk9zpNH37Hj-fq8\",\n" +
//                        "\n" +
//                        "    \"template_id\":\"69sOFuz3sdCybdJfxjyavmevce1wBZ7NMJLq08nUnKA\",\n" +
//                        "\"url\":\"http://www.baidu.com\",\n" +
//                        "\n" +
//                        "    \"topcolor\":\"#FF0000\",\n" +
//                        "\n" +
//                        "    \"data\":{\n" +
//                        "\n" +
//                        "            \"name\": {\n" +
//                        "\n" +
//                        "                \"value\":\"黄先生\",\n" +
//                        "\n" +
//                        "                \"color\":\"#173177\"\n" +
//                        "\n" +
//                        "            },\n" +
//                        "\n" +
//                        "            \"Date\":{\n" +
//                        "\n" +
//                        "                \"value\":\"06月07日 19时24分\",\n" +
//                        "\n" +
//                        "                \"color\":\"#173177\"\n" +
//                        "\n" +
//                        "            },\n" +
//                        "\n" +
//                        "            \"CardNumber\": {\n" +
//                        "\n" +
//                        "                \"value\":\"0426\",\n" +
//                        "\n" +
//                        "                \"color\":\"#173177\"\n" +
//                        "\n" +
//                        "            },\n" +
//                        "\n" +
//                        "            \"Type\":{\n" +
//                        "\n" +
//                        "                \"value\":\"消费\",\n" +
//                        "\n" +
//                        "                \"color\":\"#173177\"\n" +
//                        "\n" +
//                        "            },\n" +
//                        "\n" +
//                        "            \"Money\":{\n" +
//                        "\n" +
//                        "                \"value\":\"人民币260.00元\",\n" +
//                        "\n" +
//                        "                \"color\":\"#173177\"\n" +
//                        "\n" +
//                        "            },\n" +
//                        "\n" +
//                        "            \"DeadTime\":{\n" +
//                        "\n" +
//                        "                \"value\":\"06月07日19时24分\",\n" +
//                        "\n" +
//                        "                \"color\":\"#173177\"\n" +
//                        "\n" +
//                        "            },\n" +
//                        "\n" +
//                        "            \"Left\":{\n" +
//                        "\n" +
//                        "                \"value\":\"6504.09\",\n" +
//                        "\n" +
//                        "                \"color\":\"#173177\"\n" +
//                        "\n" +
//                        "            }\n" +
//                        "\n" +
//                        "    }\n" +
//                        "\n" +
//                        "}");
//
//       System.out.println(obj);


    }


    public static JSONObject findWebAccessToken(String code){
       return NetUtil.post("https://api.weixin.qq.com/sns/oauth2/access_token" +
                        "?appid="+APPID+"&secret="+APPSECRET+"&code="+code
                        +"&grant_type=authorization_code"
                ,"");
    }

    //websocket

    public static void findAccessToken(RedisTemplate<String,Object> redisTemplate) throws Exception{
        System.out.println("开始获得accesstoken");
        URL url = new URL("https://api.weixin.qq.com/cgi-bin/token?" +
                "grant_type=client_credential&appid="+APPID+"&secret="+APPSECRET);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();

        BufferedReader br = new BufferedReader(
                new InputStreamReader(con.getInputStream(),"utf-8")
        );

        String str;
        StringBuffer sb = new StringBuffer();
        while((str=br.readLine())!=null){
            sb.append(str);
        }

        JSONObject obj =  JSON.parseObject(sb.toString());
        accessToken = obj.getString("access_token");
        redisTemplate.opsForValue().set("access_token",accessToken,2, TimeUnit.HOURS);

        JSONObject obj2 =   NetUtil.post("https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token="+accessToken
                +"&type=jsapi","");

        String jsapi_ticket = obj2.getString("ticket");
        redisTemplate.opsForValue().set("jsapi_ticket",jsapi_ticket,2, TimeUnit.HOURS);
        System.out.println(jsapi_ticket);
        System.out.println(accessToken);
    }


}
