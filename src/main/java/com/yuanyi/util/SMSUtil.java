package com.yuanyi.util;

import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.concurrent.TimeUnit;

public class SMSUtil {
    public static boolean sendMsg(String tel, RedisTemplate<String,Object> redisTemplate){
        DefaultProfile profile = DefaultProfile.getProfile(
                "default",
                "LTAIMLC54qcvRccY",
                "Ak1DqPWmtZPuOxWVVkxY9UuXlY3Zkd");
        IAcsClient client = new DefaultAcsClient(profile);

        CommonRequest request = new CommonRequest();
        request.setMethod(MethodType.POST);
        request.setDomain("dysmsapi.aliyuncs.com");
        request.setVersion("2017-05-25");
        request.setAction("SendSms");
        request.putQueryParameter("RegionId",
                "default");
        request.putQueryParameter("PhoneNumbers",
                tel);
        request.putQueryParameter("SignName",
                "大鱼测试");
        request.putQueryParameter("TemplateCode",
                "SMS_21800103");
        String yzm = ((int)((Math.random()*9+1)*100000))+"";
        System.out.println(yzm);
        request.putQueryParameter("TemplateParam",
                "{\"code\":"+yzm+",\"name\":\""+tel+"\"}");

        redisTemplate.opsForValue().set(tel,yzm,3, TimeUnit.MINUTES);
//        try {
//            CommonResponse response =
//                    client.getCommonResponse(request);
//            System.out.println(response.getData());
//            String xx = response.getData();
//            return xx.indexOf("OK")!=-1;
//        } catch (ServerException e) {
//            e.printStackTrace();
//        } catch (ClientException e) {
//            e.printStackTrace();
//        }

        return true;
    }
}
