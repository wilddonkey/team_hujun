package com.yuanyi.exception;

/**
 * Created by Administrator on 2019/8/29.
 */
public class RepairManNotFoundException extends Exception{
    public RepairManNotFoundException(String message) {
        super(message);
    }
}
