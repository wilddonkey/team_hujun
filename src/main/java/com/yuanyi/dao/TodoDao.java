package com.yuanyi.dao;

import com.yuanyi.entity.Todo;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface TodoDao extends PagingAndSortingRepository<Todo,Integer> {
}
