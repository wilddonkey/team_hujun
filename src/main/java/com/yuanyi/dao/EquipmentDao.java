package com.yuanyi.dao;

import com.yuanyi.entity.BaoXiuMan;
import com.yuanyi.entity.Equipment;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Created by Administrator on 2019/8/28.
 */
public interface EquipmentDao  extends PagingAndSortingRepository<Equipment,Integer>,
        JpaSpecificationExecutor<Equipment> {
    Equipment findByENo(String eno);

    Iterable<Equipment> findAllByBaoXiuMan(BaoXiuMan baoXiuMan);
    Equipment findEquipmentById(Integer id);
}
