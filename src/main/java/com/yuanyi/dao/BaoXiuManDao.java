package com.yuanyi.dao;

import com.yuanyi.entity.BaoXiuMan;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Created by Administrator on 2019/8/29.
 */
public interface BaoXiuManDao extends PagingAndSortingRepository<BaoXiuMan,Integer> {
    BaoXiuMan findByOpenid(String openid);
}
