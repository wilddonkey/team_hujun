package com.yuanyi.dao;

import com.yuanyi.entity.Admin;
import org.springframework.data.repository.PagingAndSortingRepository;


public interface AdminDao extends PagingAndSortingRepository<Admin,Integer> {
    Admin findByUsernameAndPassword(String username, String password);
}
