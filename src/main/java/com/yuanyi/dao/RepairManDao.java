package com.yuanyi.dao;

import com.yuanyi.entity.RepairMan;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface RepairManDao extends PagingAndSortingRepository<RepairMan,Integer> {
    RepairMan findRepairManByTel(String tel);
    RepairMan findRepairManByOpenid(String openid);
    RepairMan findRepairManById(Integer id);
}
