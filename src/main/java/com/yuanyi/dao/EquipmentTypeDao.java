package com.yuanyi.dao;

import com.yuanyi.entity.EquipmentType;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface EquipmentTypeDao
        extends PagingAndSortingRepository<EquipmentType,Integer> {

}
