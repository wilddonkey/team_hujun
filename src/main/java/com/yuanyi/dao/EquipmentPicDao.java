package com.yuanyi.dao;

import com.yuanyi.entity.EquipmentPic;
import com.yuanyi.entity.EquipmentType;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Created by Administrator on 2019/8/28.
 */
public interface EquipmentPicDao extends PagingAndSortingRepository<EquipmentPic,Integer> {
}
