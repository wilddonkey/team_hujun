package com.yuanyi.dao;

import com.yuanyi.entity.BaoXiuDan;
import com.yuanyi.entity.Equipment;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Created by Administrator on 2019/8/28.
 */
public interface BaoXiuDanDao extends PagingAndSortingRepository<BaoXiuDan,Integer> {
}
