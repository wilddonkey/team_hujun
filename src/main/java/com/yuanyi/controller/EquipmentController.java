package com.yuanyi.controller;

import com.google.zxing.WriterException;
import com.yuanyi.dao.BaoXiuManDao;
import com.yuanyi.dao.EquipmentDao;
import com.yuanyi.dao.EquipmentPicDao;
import com.yuanyi.entity.BaoXiuMan;
import com.yuanyi.entity.Equipment;
import com.yuanyi.entity.EquipmentPic;
import com.yuanyi.entity.RepairMan;
import com.yuanyi.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.lang.Nullable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
public class EquipmentController {
    @Autowired
    EquipmentDao equipmentDao;

    @Autowired
    EquipmentPicDao equipmentPicDao;

    @Autowired
    BaoXiuManDao baoXiuManDao;

    @PostMapping("/addeq")
    @Transactional
    public Equipment addEq(@RequestBody  Equipment equipment) throws IOException, WriterException {
        System.out.println(equipment.getBaoXiuMan().getId());
        Equipment eq = equipmentDao.save(equipment);
        eq.setCreatedate(new Date());

        String count = (equipmentDao.count()+1)+"";

        int diff = 10 - count.length();
        String zero = "";
        for(int i=0;i<diff;i++){
            zero +="0";
        }
        //0000000001
        eq.seteNo(zero+count);
        eq.setBarCode(StringUtil.barCode(
                StringUtil.getWechatURL("http://java59.hfbdqn.cn/baoxiu.html?eno="
                +(zero+count+""))));



        for(EquipmentPic p:eq.getEquipmentPics()){
            p.setEquipment(eq);
            equipmentPicDao.save(p);
        }
        return eq;
    }

    @GetMapping("/findall_eq_by_baoxiuman")
    public Iterable<Equipment> findall_eq_by_baoxiuman(String openid,Integer p,int size){
        Sort sort = new Sort(Sort.Direction.DESC,"id");
        PageRequest pr =  PageRequest.of(p-1,size,sort);

        return equipmentDao.findAll(new Specification<Equipment>() {
            @Nullable
            @Override
            public Predicate toPredicate(Root<Equipment> h, CriteriaQuery<?> q, CriteriaBuilder c) {
                List<Predicate> pList = new ArrayList<>();

               pList.add(c.equal(h.get("baoXiuMan").as(BaoXiuMan.class),
                      baoXiuManDao.findByOpenid(openid)));

                Predicate[] p2 = new Predicate[pList.size()];
                q.where(c.and(pList.toArray(p2)));
                return q.getRestriction();
            }
        }, pr);

    }

    @GetMapping("/findall_eq")
    public Page<Equipment> findall_eq(Integer p, int size){
        Sort sort = new Sort(Sort.Direction.ASC,"id");
//
        PageRequest pr =  PageRequest.of(p-1,size,sort);
        // Pageable pageable = new PageRequest(p - 1, size);
        return equipmentDao.findAll(pr);
    }

    @GetMapping(value = "/sbwx_shenhe",produces = "application/json;charset=utf-8")
    @ResponseBody
    public boolean sbwx_shenhe(@RequestParam("id")Integer id,@RequestParam("status")int status){
        try{
            Equipment e = this.equipmentDao.findEquipmentById(id);
            e.setStatus(status);
            equipmentDao.save(e);
        }catch (Exception e){
            return false;
        }
        return true;
    }
}
