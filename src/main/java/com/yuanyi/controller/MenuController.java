package com.yuanyi.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.yuanyi.entity.Menu;
import com.yuanyi.entity.MenuButton;
import com.yuanyi.util.AccessTokenUtil;
import com.yuanyi.util.NetUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

@RestController
public class MenuController {
    @Resource
    RedisTemplate<String,Object> redisTemplate;

    List<MenuButton> buttons = new ArrayList<MenuButton>();
    String menu_url = " https://api.weixin.qq.com/cgi-bin/menu/create?access_token=";

    @GetMapping("/find_menu")
    public Menu find_menu(){
        Menu m = (Menu) redisTemplate.opsForValue().get("wechatMenu");
        return m;
    }

    @PostMapping("/update_menu")
    public JSONObject update_menu(@RequestBody Menu menu){
//        MenuButton btn1 = new MenuButton();
//        btn1.setName(m1);
//        btn1.setType("view");
//        btn1.setUrl("http://www.baidu.com");
//
//        MenuButton btn2 = new MenuButton();
//        btn2.setName(m2);
//        btn2.setType("view");
//        btn2.setUrl("http://www.baidu.com");
//
//        MenuButton btn3 = new MenuButton();
//        btn3.setName(m3);
//        btn3.setType("view");
//        btn3.setUrl("http://www.baidu.com");
//
//        buttons.add(btn1);
//        buttons.add(btn2);
//        buttons.add(btn3);
//
//        Menu menu = new Menu();
//        menu.setButton(buttons);

//        menu.getButton().get(0).
//                setUrl("https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx9cfb23748200cfe0&redirect_uri=http://java59.hfbdqn.cn/index.html&response_type=code&scope=snsapi_base&state=STATE#wechat_redirect");
//
//        menu.getButton().get(2).
//                setUrl("https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx9cfb23748200cfe0&redirect_uri=http://java59.hfbdqn.cn/repair_man.html&response_type=code&scope=snsapi_base&state=STATE#wechat_redirect");

        System.out.println(JSON.toJSONString(menu));
        JSONObject obj = NetUtil.post(menu_url+
                        redisTemplate.opsForValue().get("access_token"),
                JSON.toJSONString(menu));

        redisTemplate.opsForValue().set("wechatMenu",menu);

       return obj;
    }
}
