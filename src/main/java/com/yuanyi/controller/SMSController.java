package com.yuanyi.controller;

import com.yuanyi.util.SMSUtil;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class SMSController {
    @Resource
    RedisTemplate<String,Object> redisTemplate;

    @GetMapping("sendsms")
    public boolean sendSms(String tel){
       return  SMSUtil.sendMsg(tel,redisTemplate);
    }
}
