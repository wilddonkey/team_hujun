package com.yuanyi.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

@RestController
public class EquipmentPicController {
    public static void main(String[] args) {
        System.out.println(System.getProperty("os.name"));
    }

    @PostMapping("/epupload")
    public String upload(MultipartFile file) throws IOException {

        String oldFileName = file.getOriginalFilename();
        int lastDotIndex = oldFileName.lastIndexOf(".");
        String ext = oldFileName.substring(lastDotIndex);
        System.out.println(file);
        boolean isWin = System.getProperty("os.name").indexOf("Windows")>=0;
        String fn = UUID.randomUUID().toString()+ext;
        String fileName = "/upload/"+fn;
        if(isWin){
            fileName = "c://upload/"+fn;
        }

        file.transferTo(new File(fileName));
        return "http://java59.hfbdqn.cn/findimg?fileName="+fn;
    }
}

