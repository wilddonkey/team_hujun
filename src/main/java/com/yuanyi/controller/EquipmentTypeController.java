package com.yuanyi.controller;

import com.yuanyi.dao.EquipmentTypeDao;
import com.yuanyi.entity.EquipmentType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class EquipmentTypeController {
    @Autowired
    EquipmentTypeDao equipmentTypeDao;

    @GetMapping("/find_all_et")
    public Iterable<EquipmentType> find_all_et(){
        return equipmentTypeDao.findAll();
    }
}
