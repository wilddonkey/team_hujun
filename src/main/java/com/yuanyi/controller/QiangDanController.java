package com.yuanyi.controller;

import com.yuanyi.WebSocketServer;
import com.yuanyi.dao.BaoXiuDanDao;
import com.yuanyi.dao.RepairManDao;
import com.yuanyi.entity.BaoXiuDan;
import com.yuanyi.entity.RepairMan;
import com.yuanyi.exception.BaoXianDanNoErrorException;
import com.yuanyi.exception.RepairManNotFoundException;
import com.yuanyi.util.NetUtil;
import org.redisson.Redisson;
import org.redisson.api.RLock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
public class QiangDanController {
    @Autowired
    RepairManDao repairManDao;

    @Autowired
    BaoXiuDanDao baoXiuDanDao;

    @Autowired
    Redisson redisson;

    // redisson
    @GetMapping("/qiangdan")
    public boolean qiangDan(int id,String openid) throws BaoXianDanNoErrorException,
            RepairManNotFoundException {
        RLock lock = redisson.getLock("Hello");
        boolean isok = lock.tryLock();

        //如果这个机器且这个线程获得了锁
        if(isok) {
            try{
                BaoXiuDan bxd = baoXiuDanDao.findById(id).get();
                if (bxd == null) {
                    throw new BaoXianDanNoErrorException("报修单编号有误！");
                }

                if (bxd.getRepairMan() == null) {
                    RepairMan repairMan = repairManDao.findRepairManByOpenid(openid);
                    if (repairMan == null) {
                        throw new RepairManNotFoundException("不存这个工程师！");
                    }

                    bxd.setRepairMan(repairMan);
                    baoXiuDanDao.save(bxd);
                    System.out.println(openid + "成功");
                    WebSocketServer.sendInfo(
                            repairMan.getName()+"已经接单！"+bxd.getId(),
                            bxd.getBaoXiuMan().getOpenid());
                    return true;
                } else {
                    System.out.println(openid + "失败");
                    return false;
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally{
                lock.unlock();
            }
        }

        return false;
    }

    public static void main(String[] args){
        String[] openids = {
                "oSLehjv6urj7So8KV9zgxfAY17N8",
                "oSLehjilZ5H2tCPm6rN1qOBJZP1c",
                "oSLehjsGO3l1tpDPUN0J0xJF7WIg",
                "oSLehjsJ1TS9ABV0ZwQraojbJToU",
                "oSLehju0XHzjg5j49BegEd4bNWQs"
        };

        for(int i=0;i<5;i++) {
            String openid =openids[i];
            new Thread(new Runnable() {
                @Override
                public void run() {
                    double rnum = Math.random();
                    System.out.println(rnum);
                    String s = (rnum>0.5)?":8080":":81";
                    String url = "http://localhost"+s+"/qiangdan?id=481&openid="+openid;
                    System.out.println(url);
                    NetUtil.get(url);
                }
            }).start();
        }
    }
}
