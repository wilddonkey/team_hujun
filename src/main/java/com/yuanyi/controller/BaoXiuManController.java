package com.yuanyi.controller;

import com.yuanyi.dao.BaoXiuManDao;
import com.yuanyi.entity.BaoXiuMan;
import com.yuanyi.entity.RepairMan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

@RestController
public class BaoXiuManController {
    @Autowired
    BaoXiuManDao baoXiuManDao;

    @GetMapping(value = "/find_all_baoxiuman",produces = "application/json;charset=utf-8")
    @ResponseBody
    public Page<BaoXiuMan> findAll(@RequestParam("p") Integer p, @RequestParam("size") Integer size){
        Sort sort = new Sort(Sort.Direction.ASC,"id");
        PageRequest pr =  PageRequest.of(p-1,size,sort);
        //Pageable pageable = new PageRequest(p - 1, size);
        return baoXiuManDao.findAll(pr);
    }

    @GetMapping(value = "/bxm_shenhe",produces = "application/json;charset=utf-8")
    @ResponseBody
    public boolean bxm_shenhe(@RequestParam("bxm_id") Integer bxm_id,@RequestParam("status") Integer status,
                              @RequestParam(value="rejectMsg", required=false) String rejectMsg){
        try{
            BaoXiuMan bxm = baoXiuManDao.findById(bxm_id).get();
            bxm.setStatus(status);
            bxm.setRejectMsg(rejectMsg);
            baoXiuManDao.save(bxm);
        }catch (Exception ex){
            return false;
        }
        return true;
    }

    @PostMapping("/baoxian_man_bind")
    public BaoXiuMan baoxian_man_bind(@RequestBody  BaoXiuMan baoXiuMan){
        return baoXiuManDao.save(baoXiuMan);
    }

    @GetMapping("/find_baoxiuman_byopenid")
    public BaoXiuMan find_baoxiuman_byopenid(String openid){

        return baoXiuManDao.findByOpenid(openid);
    }
}
