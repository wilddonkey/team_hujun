package com.yuanyi.controller;

import com.yuanyi.dao.AdminDao;
import com.yuanyi.entity.Admin;
import com.yuanyi.util.VerifyUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.image.BufferedImage;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

@RestController
public class AdminController {
    @Autowired
    private AdminDao adminDao;

  /*  @PostMapping(value = "/login", produces = "application/json;charset=UTF-8")
    @ResponseBody
    public Map login(@RequestParam("username") String username, @RequestParam("password") String password, @RequestParam("code") String code, HttpServletResponse response, HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        Admin admin = adminDao.findByUsernameAndPassword(username, password);
        map.put("result", admin != null);
        map.put("admin", admin);
        System.out.println(admin + ">>>>>>>>>>>>>>>>>>>>>");
        return map;
    }*/

    @PostMapping(value = "/login", produces = "application/json;charset=UTF-8")
    @ResponseBody
    public Map login(@RequestParam("username") String username, @RequestParam("password") String password, @RequestParam("code") String code, HttpServletResponse response, HttpServletRequest request) {
        Map<String,Object> map=new HashMap<>();
        HttpSession session=request.getSession();
        if(session.getAttribute("imageCode")==null){
            map.put("result",false);
            map.put("msg","请重新获取验证码");
        }else {
            if(session.getAttribute("imageCode").toString().equalsIgnoreCase(code)){
                Admin admin = adminDao.findByUsernameAndPassword(username, password);
                if (admin == null) {
                    map.put("result", admin != null);
                    map.put("admin", admin);
                    map.put("msg", "登录失败");
                } else {
                    map.put("result", admin != null);
                    map.put("admin", admin);
                    map.put("msg", "登录成功");
                }
            }else {
                map.put("result",false);
                map.put("msg","验证码错误");
            }
        }
        return map;
    }

    @GetMapping("/getcode")
    public void getCode(HttpServletResponse response, HttpServletRequest request) throws Exception {
        HttpSession session = request.getSession();
        //利用图片工具生成图片
        //第一个参数是生成的验证码，第二个参数是生成的图片
        Object[] objs = VerifyUtil.createImage();
        //将验证码存入Session
        session.setAttribute("imageCode", objs[0]);
        //将图片输出给浏览器
        BufferedImage image = (BufferedImage) objs[1];
        response.setContentType("image/png");
        OutputStream os = response.getOutputStream();
        ImageIO.write(image, "png", os);
    }


}