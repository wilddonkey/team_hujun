package com.yuanyi.entity;

import javax.persistence.*;

@Entity
public class Comment {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer id;

    private double score;

    private String content;

    @OneToOne
    private BaoXiuDan baoXiuDan;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public BaoXiuDan getBaoXiuDan() {
        return baoXiuDan;
    }

    public void setBaoXiuDan(BaoXiuDan baoXiuDan) {
        this.baoXiuDan = baoXiuDan;
    }
}
