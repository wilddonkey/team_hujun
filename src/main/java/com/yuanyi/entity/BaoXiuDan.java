package com.yuanyi.entity;

import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;

@Entity
public class BaoXiuDan {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer id;

    @OneToOne
    private Equipment equipment;

    private String guzhang;

    @OneToOne
    private RepairMan repairMan;

    @OneToOne
    private BaoXiuMan baoXiuMan;

    /**
     * 维修状态
     * 0.未维修 1.维修工程师正在赶来，2.已经到达，
     * 3.正在维修，4.更换配件...，5.维修完成，-1 维修失败
     */
    @ColumnDefault("0")
    private Integer status;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public BaoXiuMan getBaoXiuMan() {
        return baoXiuMan;
    }

    public void setBaoXiuMan(BaoXiuMan baoXiuMan) {
        this.baoXiuMan = baoXiuMan;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Equipment getEquipment() {
        return equipment;
    }

    public void setEquipment(Equipment equipment) {
        this.equipment = equipment;
    }

    public String getGuzhang() {
        return guzhang;
    }

    public void setGuzhang(String guzhang) {
        this.guzhang = guzhang;
    }

    public RepairMan getRepairMan() {
        return repairMan;
    }

    public void setRepairMan(RepairMan repairMan) {
        this.repairMan = repairMan;
    }
}
