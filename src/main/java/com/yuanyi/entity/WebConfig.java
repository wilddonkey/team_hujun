package com.yuanyi.entity;

import com.yuanyi.util.AccessTokenUtil;

/**
 * Created by Administrator on 2019/8/26.
 */
public class WebConfig {
    String appId= AccessTokenUtil.APPID; // 必填，公众号的唯一标识
    String timestamp= ""; // 必填，生成签名的时间戳
    String nonceStr= ""; // 必填，生成签名的随机串
    String signature="";// 必填，签名

    public WebConfig(String timestamp, String nonceStr, String signature) {
        this.timestamp = timestamp;
        this.nonceStr = nonceStr;
        this.signature = signature;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getNonceStr() {
        return nonceStr;
    }

    public void setNonceStr(String nonceStr) {
        this.nonceStr = nonceStr;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }
}
