package com.yuanyi.entity;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Administrator on 2019/8/26.
 */
public class MenuButton  implements  Serializable{
    private String type;
    private String name;
    private String url;
    private List<MenuButton> sub_button;

    public List<MenuButton> getSub_button() {
        return sub_button;
    }

    public void setSub_button(List<MenuButton> sub_button) {
        this.sub_button = sub_button;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
