package com.yuanyi.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2019/8/26.
 */
public class Menu implements  Serializable{
    private List<MenuButton> button = new ArrayList<MenuButton>();

    public List<MenuButton> getButton() {
        return button;
    }

    public void setButton(List<MenuButton> button) {
        this.button = button;
    }
}
