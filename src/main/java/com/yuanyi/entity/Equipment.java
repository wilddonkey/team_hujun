package com.yuanyi.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
public class Equipment {
    @Override
    public String toString() {
        return "Equipment{" +
                "id=" + id +
                ", eNo='" + eNo + '\'' +
                ", createdate=" + createdate +
                ", equipmentType=" + equipmentType +
                ", equipmentPics=" + equipmentPics +
                ", name='" + name + '\'' +
                '}';
    }

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer id;

    private int status;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * 设备编号
     */
    private String eNo;

    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdate;

    @ManyToOne
    @JoinColumn(name = "equipment_type_id")
    private EquipmentType equipmentType;

    @OneToMany(cascade = CascadeType.ALL,mappedBy = "equipment")
    private List<EquipmentPic> equipmentPics;

    @ManyToOne
    @JoinColumn(name = "baoxiuman_id")
    private BaoXiuMan baoXiuMan;


    public BaoXiuMan getBaoXiuMan() {
        return baoXiuMan;
    }

    public void setBaoXiuMan(BaoXiuMan baoXiuMan) {
        this.baoXiuMan = baoXiuMan;
    }

    private String name;

    private String barCode;

    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    public List<EquipmentPic> getEquipmentPics() {
        return equipmentPics;
    }

    public void setEquipmentPics(List<EquipmentPic> equipmentPics) {
        this.equipmentPics = equipmentPics;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String geteNo() {
        return eNo;
    }

    public void seteNo(String eNo) {
        this.eNo = eNo;
    }

    public Date getCreatedate() {
        return createdate;
    }

    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }

    public EquipmentType getEquipmentType() {
        return equipmentType;
    }

    public void setEquipmentType(EquipmentType equipmentType) {
        this.equipmentType = equipmentType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
