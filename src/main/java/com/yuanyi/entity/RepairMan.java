package com.yuanyi.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class RepairMan {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer id;
    private String name;
    private String openid;
    private String tel;
    private String yzm;
    private String id_zheng;
    private String id_fan;
    private String person_pic;
    private String rejectMsg;
    private int status;

    public String getRejectMsg() {
        return rejectMsg;
    }

    public void setRejectMsg(String rejectMsg) {
        this.rejectMsg = rejectMsg;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getId_zheng() {
        return id_zheng;
    }

    public void setId_zheng(String id_zheng) {
        this.id_zheng = id_zheng;
    }

    public String getId_fan() {
        return id_fan;
    }

    public void setId_fan(String id_fan) {
        this.id_fan = id_fan;
    }

    public String getPerson_pic() {
        return person_pic;
    }

    public void setPerson_pic(String person_pic) {
        this.person_pic = person_pic;
    }

    public String getYzm() {
        return yzm;
    }

    public void setYzm(String yzm) {
        this.yzm = yzm;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }
}
